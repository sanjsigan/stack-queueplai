package java.ass.stack;

public class MyStack {
	private int maxsize;
	String reverse = "";
	char[] stackArray = reverse.toCharArray();
	private int top;

	public MyStack(int m) {
		maxsize = m;
		stackArray = new char[m];
		top = -1;

	}

	public void push(char j) {
		stackArray[++top] = j;

	}

	public char pop() {
		return stackArray[top--];

	}

	public char peek() {
		return stackArray[top];

	}

	public boolean isempty() {
		return (top == -1);

	}

	public boolean isfull() {
		return (top == maxsize - 1);

	}
}
