package java.ass.stack;

import java.util.Scanner;

public class PalindromeDemo {
	public static void main(String[] args) {
		String popString = "";
		String DequeueString = "";
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the value");
		String read = scan.nextLine();
		String name = read.toUpperCase();

		System.out.println("your value is : " + name);
		MyStack reverseStack = new MyStack(10);
		MyQueue queue = new MyQueue(10);

		for (int i = 0; i < name.length(); i++) {
			reverseStack.push(name.charAt(i));

		}
		for (int i = 0; i < name.length(); i++) {
			queue.enqueue(name.charAt(i));
		}
		while (!reverseStack.isempty() && !queue.isEmpty()) {
			popString = popString + Character.toString(reverseStack.pop());
			DequeueString = DequeueString + Character.toString(queue.dequeue());
		}

		System.out.println();

		if (popString.equals(name) && DequeueString.equals(name)) {
			System.out.println(name + " is a palindrome");
		} else {
			System.out.println(name + " is not palindrome");
		}
	}
}
